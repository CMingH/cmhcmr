<?php


namespace cmh\facade;


use cmh\service\TreeService;
use think\Facade;

/**
 * Class Farm
 *
 * @package app\api\facade
 */

class Tree extends Facade
{
    protected static function getFacadeClass()
    {
        return TreeService::class;
    }
}
