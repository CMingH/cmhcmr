<?php


namespace cmh\facade;


use cmh\service\AliyunsmsService;
use think\Facade;

/**
 * Class Farm
 *
 * @package app\api\facade
 */

class Aliyunsms extends Facade
{
    protected static function getFacadeClass()
    {
        return AliyunsmsService::class;
    }
}
